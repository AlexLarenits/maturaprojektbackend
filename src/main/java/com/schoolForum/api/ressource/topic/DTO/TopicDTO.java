package com.schoolForum.api.ressource.topic.DTO;

import com.schoolForum.api.model.Account;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TopicDTO {

    private String name;
}
